function rd --description 'Similar to rustdoc but also includes private docs.'
	cargo rustdoc --open -- --no-defaults --passes collapse-docs --passes unindent-comments --passes strip-priv-imports
end
