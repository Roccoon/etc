function plx --description 'run perl app with local::lib in ./lib'
	set -l app $argv[1]
    set -e argv[1]
	pl lib/bin/$app $argv
end
