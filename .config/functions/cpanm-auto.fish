function cpanm-auto --description 'find cpanm automatically, or run it online if not found'
	if test -x $HOME/perl5/bin/cpanm
        eval $HOME/perl5/bin/cpanm $argv
    else
        curl -L cpanmin.us | perl - $argv
    end
end
