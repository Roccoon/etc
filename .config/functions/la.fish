function la --description 'List contents of directory, including hidden files in directory using long format'
	l -a $argv
end
