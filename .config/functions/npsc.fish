function npsc --description 'npsudo systemctl, with auto daemon reload'
	npsudo systemctl $argv
    or begin
        echo -e "\e[1;32mdaemon reload...\e[0m"
        npsudo systemctl daemon-reload
        echo -e "\e[1mretry...\e[0m"
        npsudo systemctl $argv
    end
end

