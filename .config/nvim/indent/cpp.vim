" cpp
if exists("b:did_giumo_cpp_indent")
    finish
end
let b:did_giumo_cpp_indent = 1

set cinoptions=l1,Ls,g0,t0,c0,(s,U1,k2s,j1
set indentexpr=S_giumo_indent_cpp()
set indentkeys+=0>

func! S_giumo_indent_cpp()
    let cline_num = line('.')
    let cline = getline(cline_num)
    let pline_num = prevnonblank(cline_num - 1)
    let pline = getline(pline_num)
    let pindent = indent(pline_num)

    if pline =~# '^\s*template\>.*<\s*$'
        return pindent + &shiftwidth
    endif
    if pline =~# '^\s*template\>'
        return pindent
    endif
    if pline =~# '^\s*>\s*$'
        return pindent
    endif
    if cline =~# '^\s*>\s*$'
        return pindent - &shiftwidth
    endif

    return cindent('.')
endf

