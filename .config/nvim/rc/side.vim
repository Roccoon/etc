" side: the interactive surround.vim
"   operate on both sides of a visual selection
if exists("g:loaded_rc_side")
    finish
endif
let g:loaded_rc_side = 1

vnoremap s :call <SID>invoke()<CR>

let s:break_loop = {
    \ "\<cr>": 1,
    \ "\<esc>": 1,
\}

let s:pairs = {
    \ "<": [ "<", ">" ],
    \ "(": [ "(", ")" ],
    \ "[": [ "[", "]" ],
    \ "{": [ "{", "}" ],
    \ "}": [ "{ ", " }" ],
    \ "]": [ "[ ", " ]" ],
    \ ")": [ "( ", " )" ],
    \ ">": [ "< ", " >" ],
\}

func! s:invoke() range
    while 1
        echohl ModeMsg | echon "-- SIDE --" | echohl None
        let x = s:getchar()
        call s:dispatch(x)
        if has_key(s:break_loop, x)
            break
        endif
        redraw
    endw
endf

func! s:dispatch(x)
    " return key or esc to cancel
    if a:x == "\<cr>" || a:x == "\<esc>"
        return
    endif

    " backspace key to remove 1 char per side for both sides
    if a:x == "\<bs>"
        return s:change_side(1, 1, "", "")
    endif

    if has_key(s:pairs, a:x)
        let [left, right] = s:pairs[a:x]
        call s:change_side(0, 0, left, right)
    else
        call s:change_side(0, 0, a:x, a:x)
    endif
endf

func! s:change_side(lkill, rkill, prepend, append)
    let text = s:visual_selected_text()
    let text = s:side_op(text, a:lkill, a:rkill, a:prepend, a:append)
    call s:replace_visual_selection(text)
endf

" side operator: kill and then add some text on both sides of <text>
func! s:side_op(text, lkill, rkill, prepend, append)
    let text = a:text
    let len = strlen(text)
    let nkill = a:lkill + a:rkill
    if nkill >= len
        let text = ""
    else
        let text = strpart(text, a:lkill, len-nkill)
    endif
    return a:prepend . text . a:append
endf

func! s:visual_selected_text()
    let a = @a
    try
        silent normal! gv"ay
        return @a
    finally
        let @a = a
    endt
endf

func! s:replace_visual_selection(text)
    let a = @a
    try
        let @a = a:text
        silent normal! gv"apgv
    finally
        let @a = a
    endt
endf

func! s:getchar()
    let c = getchar()
    if c =~ '^\d\+$'
        return nr2char(c)
    else
        return c
    endif
endf

