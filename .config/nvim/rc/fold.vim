if exists("g:loaded_rc_fold")
    finish
endif
let g:loaded_rc_fold = 1

set foldlevel=999
set foldmethod=indent
set foldminlines=0
set foldtext=FOLD_fold_text()

" (S)-TAB       (close/)open fold (all descendants)
" BAR           unfold current fold, fold all others
" F2            unfold all
" F3            fold all
" F7            toggle fold
nmap <TAB> zO
nmap <S-TAB> zc
nmap <bar> zMzv
nmap <F2> zR
nmap <F3> zM
nmap <F7> zi

" closed folds will look like this:
"   ▸ original_text();                             · 1234 ▾|
"   ▸ original_text();                             ·· 234 ▾|
"   ▸ original_text();                             ··· 34 ▾|
"   ▸ original_text();                             ···· 1 ▾|
"   ▸ original_text();                            · 12345 ▾|
"   ▸ original_text();                           · 123456 ▾|
"                                                          `- the right edge of the window
func! FOLD_fold_text()
    let line = getline(v:foldstart)
    let num_folded_lines = v:foldend - v:foldstart + 1
    let num_folded_lines_indicator = " ·" . repeat("·", 4-strwidth(num_folded_lines)) . " " . num_folded_lines . " ▾"

    let left_pane_width = &numberwidth*&number + &foldcolumn
    let area_width = winwidth(0) - left_pane_width
    let prefix_width = area_width - strwidth(num_folded_lines_indicator)
    if prefix_width < 1
        return line
    endif

    let prefix = line[0:prefix_width-1]
    let prefix = prefix . repeat(" ", prefix_width - strwidth(prefix))
    let prefix = substitute(prefix, "\\s\\s\\ze\\S", "▸ ", "")
    return prefix . num_folded_lines_indicator
endf

