" xc: extended c & c++
if exists("g:loaded_rc_xc")
    finish
endif
let g:loaded_rc_xc = 1

" completion scans C++ include files
set path+=/usr/include/c++/*/

" Includes:
"   I <filename>        include <filename> as local header (e.g. #include "hello.h")
"   IS <filename>       include <filename> as system header (e.g. #include <hello.h>)
"   II                  include IO library.
"
" Namespaces:
"   NS <name>           create namespace <name>
"   NS                  create anonymous namespace
"
" Hacks:
"   UU <var>            mark <var> as unused
command -nargs=1 -complete=file I call <SID>include("<args>")
command -nargs=1 -complete=file_in_path IS call <SID>include_system("<args>")
command II call <SID>include_io()
command -nargs=? NS call <SID>namespace(<q-args>)
command -nargs=1 CR call <SID>const_ref("<args>")
command -nargs=1 UU call <SID>unused("<args>")

func! s:include(file)
    exe "normal o#include \"" . a:file . "\""
    exe "normal ^www"
endf

func! s:include_system(file)
    exe "normal o#include <" . a:file . ">"
    exe "normal ^www"
endf

func! s:include_io()
    if &ft == "cpp"
        IS iostream
        exe "normal ousing std::cin;"
        exe "normal ousing std::cout;"
        exe "normal ousing std::cerr;"
        exe "normal ousing std::endl;"
    elseif &ft == "c"
        IS stdio.h
    endif
endf

func! s:namespace(name)
    if a:name == ""
        exe "normal onamespace\n{\n}"
    else
        exe "normal onamespace " . a:name . "\n{\n}"
    endif
    exe "normal %"
endf

func! s:unused(vars)
    exe "normal o \b#pragma unused (" . a:vars . ")"
endf

