" setup terminal for use by neovim
if exists("g:loaded_rc_terminal")
    finish
endif
let g:loaded_rc_terminal = 1

let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
set termguicolors

