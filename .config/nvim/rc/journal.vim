" for logging of plugins
" should be loaded first
if exists("g:loaded_rc_journal")
    finish
endif
let g:loaded_rc_journal = 1

command Journal call journal#show()

func! journal#buffer(name)
    call s:buffer(a:name)
endf

func! journal#write(tag, message)
    call s:write(a:tag, a:message)
endf

func! journal#show()
    call s:show()
endf

func! s:buffer(name)
    let s:name = tempname()
    let s:buffer = bufnr(s:name, 1)
    silent call nvim_buf_set_lines(s:buffer, 0, -1, v:true, split(a:name, "\n"))
    " FIXME: the following not working, why?
    "silent call nvim_buf_set_option(s:buffer, "buftype", "nowrite")
    silent call nvim_buf_set_option(s:buffer, "modified", v:false)
    silent call nvim_buf_set_option(s:buffer, "modifiable", v:false)
    silent call nvim_buf_set_option(s:buffer, "readonly", v:true)
endf
call s:buffer("JOURNAL")

func! s:write(tag, message)
    let message = s:format(a:tag, a:message)
    call s:write_raw(message)
endf

func! s:show()
    exe 'tabe' s:name
endf

func! s:format(tag, message)
    let prefix = s:now() . " [" . a:tag . "] "
    let message = s:indent_rest(a:message, strdisplaywidth(prefix))
    return prefix . message
endf

func! s:now()
    let time = localtime()
    let format = "%Y-%m-%d %H:%M:%S"
    return strftime(format, time)
endf

func! s:indent_rest(str, indent_level)
    let indent = repeat(' ', a:indent_level)
    return join(split(a:str, "\n"), "\n" . indent)
endf

func! s:write_raw(str)
    try
        silent call nvim_buf_set_option(s:buffer, "modifiable", v:true)
        silent call nvim_buf_set_lines(s:buffer, -1, -1, v:true, split(a:str, "\n"))
    finally
        silent call nvim_buf_set_option(s:buffer, "modified", v:false)
        silent call nvim_buf_set_option(s:buffer, "modifiable", v:false)
        silent call nvim_buf_set_option(s:buffer, "readonly", v:true)
    endt
endf

