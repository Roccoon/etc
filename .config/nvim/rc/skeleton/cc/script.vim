let basename = expand("%:t:r")
let suffix = substitute(expand("%:e"), 'c', 'h', 'g')
exe "%s/BASENAME/" . basename . "/g"
exe "%s/SUFFIX/" . suffix . "/g"
call skeleton#cursor_line("CURSOR-PREV-LINE", -1)

