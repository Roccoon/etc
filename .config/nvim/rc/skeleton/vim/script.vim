let name = expand("%:t:r")
let tag = toupper(name)
exe "%s/NAME/" . name . "/g"
exe "%s/TAG/" . tag . "/g"
call skeleton#cursor_line("CURSOR-PREV-LINE", -1)

