let s:root = expand("%:r")
if strpart(s:root, 0, 2) == "./"
    let s:root = strpart(s:root, 2)
endif
if strpart(s:root, 0, 4) == "lib/"
    let s:root = strpart(s:root, 4)
endif
if strpart(s:root, 0, 1) != "/"
    let s:package = substitute(s:root, "/", "::", "g")
    exe "%s/PACKAGE/" . s:package . "/g"
endif
call skeleton#cursor_line("CURSOR-PREV-LINE", -1)

