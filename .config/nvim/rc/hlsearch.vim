" highlight search disabler
if exists("g:loaded_rc_hlsearch")
    finish
endif
let g:loaded_rc_hlsearch = 1

" clear hlsearch and redraw screen
nnoremap <silent><C-l> :nohls<CR><C-l>

au InsertEnter * call <SID>disable()
au InsertLeave * call <SID>resume()

func! s:disable()
    let s:old = &hlsearch
    let &hlsearch = 0
endf

func! s:resume()
    let &hlsearch = s:old
endf

