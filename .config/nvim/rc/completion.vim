" setup completion
if exists("g:loaded_rc_completion")
    finish
endif
let g:loaded_rc_completion = 1

" allow find under current directory
set path+=**

" completion also scan include files, but no tags
set complete+=i
set complete-=t
set completeopt+=longest

" make completion always case sensitive
au InsertEnter * set noignorecase
au InsertLeave * set ignorecase

