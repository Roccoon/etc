" read mode: only highlight declaration headers
if exists("g:loaded_rc_readmode")
    finish
endif
let g:loaded_rc_readmode = 1

func! s:read_mode()
    syn clear
    syn match Statement /\v^(.*\s)?(class|struct)\s.*$/
    syn match Function /\v^(.*\s)?(func|function|func!|def)\s.*$/
    syn region Comment start=/^\s*\/\// end=/$/ fold
    syn region Comment start=/\/\*/ end=/\*\// fold
    syn keyword Constant if else for foreach
    syn keyword Title return throw raise break continue next last
    setlocal foldmethod=syntax
    setlocal foldlevel=0
endf

command ReadMode call <SID>read_mode()

