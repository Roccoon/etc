" neovim builtin terminal emulator configuration
if exists("g:loaded_terminal_emulator")
    finish
endif
let g:loaded_terminal_emulator = 1

" better keybinding
tnoremap <Esc> <C-\><C-n>
tnoremap <Esc><Esc> <Esc>
tnoremap <Esc>w <C-\><C-n><C-w><C-w>
"tnoremap <C-c> <C-\><C-n>
"tnoremap <C-c><C-c> <C-c>

" Terminal Color Scheme: Giumo modified from XTerm
let g:terminal_color_0  = '#000000'
let g:terminal_color_1  = '#ee4444'
let g:terminal_color_2  = '#00cd00'
let g:terminal_color_3  = '#cdcd00'
let g:terminal_color_4  = '#6868e4'
let g:terminal_color_5  = '#ff53ff'
let g:terminal_color_6  = '#00cdcd'
let g:terminal_color_7  = '#e5e5e5'
let g:terminal_color_8  = '#7f7f7f'
let g:terminal_color_9  = '#ff6c6c'
let g:terminal_color_10 = '#00ff00'
let g:terminal_color_11 = '#ffff00'
let g:terminal_color_12 = '#6e6ece'
let g:terminal_color_13 = '#f272f2'
let g:terminal_color_14 = '#00ffff'
let g:terminal_color_15 = '#ffffff'

