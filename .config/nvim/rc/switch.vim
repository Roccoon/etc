" switch: switching between files
" nmap <C-s> for switching header and impls
" nmap <C-c><C-s> for switch to a new file
if exists("g:loaded_rc_switch")
    finish
endif
let g:loaded_rc_switch = 1

command! -nargs=? SwitchCreate call <SID>switch_create(<q-args>)
command! -nargs=0 Switch call <SID>switch()

nmap <C-s> :Switch<CR>
nmap <C-c><C-s> :SwitchCreate<SPACE>

let s:switch_queue = [
            \ "pull", "inl",
            \ "hh", "hxx", "hpp", "h++", "H", "", "h",
            \ "cc", "cxx", "cpp", "c++", "C", "c",
            \ ]

func! s:switch()
    let root = expand("%:r")
    let suffix = expand("%:e")

    let idx = index(s:switch_queue, suffix)
    if idx == -1
        echohl ErrorMsg
        echo "Suffix \"" . suffix . "\" not in switch queue."
        echohl None
        return
    endif

    let idx = s:next_index(idx)
    while suffix != s:switch_queue[idx]
        let new_path = root . "." . s:switch_queue[idx]
        if filereadable(new_path)
            exe "edit " . new_path
            return
        endif
        let idx = s:next_index(idx)
    endw

    call feedkeys(":SwitchCreate\<SPACE>")
endf

func! s:switch_create(suffix)
    let root = expand("%:r")
    if a:suffix == ''
        let new_path = root
    else
        let new_path = root . "." . a:suffix
    endif
    exe "edit " . new_path
endf

func! s:next_index(idx)
    let max = len(s:switch_queue)
    return (a:idx + 1) % max
endf

