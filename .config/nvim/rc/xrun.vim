" xrun: compile & run
if exists("g:loaded_rc_xrun")
    finish
endif
let g:loaded_rc_xrun = 1

nnoremap <F4> :call <SID>compile_and_test()<CR>
nnoremap <F5> :call <SID>compile_and_run()<CR>
nnoremap <F6> :call <SID>make_test()<CR>
nnoremap <F8> :call <SID>make_rebuild()<CR>
nnoremap <F9> :call <SID>configure()<CR>
imap <F4> <C-o><F4>
imap <F5> <C-o><F5>
au FileType cpp call <SID>update_flags()

func! s:compile_and_test()
    w
    if &filetype == "c"
        call s:tabterm("gcc -Wall -pthread -std=gnu11 -Og -ggdb -march=native -lm -o %:r % && gdb ./%:r")
    elseif &filetype == "rust"
        call s:tabterm("cargo test --all -- --nocapture")
    else
        call s:tabterm("echo -e \\\\e[1\\;33munknown filetype for compile-and-test, fallback to makefile\\\\e[0m; make test -j4 -C $(git rev-parse --show-toplevel 2>/dev/null \|\| echo .)")
    endif
endf

func! s:compile_and_run()
    w

    let fullname = expand("%")
    let rootname = expand("%:r")    | " a.k.a. strips off suffix names

    let xfullname = shellescape(fullname)
    let xrootname = shellescape(rootname)

    if &filetype == "c"
        call s:tabterm("gcc -Wall -pthread -std=gnu11 -O3 -march=native -lm -o " . xrootname . " " . xfullname . " && ./" . xrootname)
    elseif &filetype == "cpp"
        call s:tabterm("ml -Cn5 " . xfullname)
    elseif &filetype == "java"
        call s:tabterm("javac " . xfullname . " && java " . xrootname)
    elseif &filetype == "dot"
        exe "!dot -T png -o /tmp/dot-output.png % && feh /tmp/dot-output.png"
    elseif &filetype == "lua"
        call s:tabterm("luajit " . xfullname)
    elseif &filetype == "perl"
        call s:tabterm("perl " . xfullname)
    elseif &filetype == "sh"
        call s:tabterm("sh " . xfullname)
    elseif &filetype == "javascript"
        call s:tabterm("node --es_staging " . xfullname)
    elseif &filetype == "python"
        call s:tabterm("python " . xfullname)
    elseif &filetype == "ruby"
        exe "!ruby %"
    elseif &filetype == "asciidoc"
        call s:tabterm("asciidoc -b html5 -o /tmp/asciidoc.html " . xfullname . " && elinks /tmp/asciidoc.html")
    elseif &filetype == "html"
        call s:tabterm("elinks " . xfullname)
    elseif &filetype == "scheme"
        exe "!bigloo -o %< % && ./%<"
    elseif &filetype == "rust"
        call s:tabterm("cargo run")
    elseif &filetype == "nim"
        call s:tabterm("nim -d:vim -o:/tmp/nim-build -r -d:ssl c " . xfullname)
    else
        echo "unknown filetype for compile-and-run"
        echo "  fallback to makefile"
        make! -C $(git rev-parse --show-toplevel 2>/dev/null \|\| echo .) test -j4
    endif
endf

func! s:configure()
    w
    let prefix = s:find_file("configure")
    call s:tabterm("cd " . prefix . "; ./configure")
endf

func! s:make_test()
    w
    let prefix = s:find_file("makefile")
    call s:tabterm("cd " . prefix . "; make test -j4")
endf

func! s:make_rebuild()
    w
    let prefix = s:find_file("makefile")
    call s:tabterm("cd " . prefix . "; make rebuild -j4")
endf

func! s:update_flags()
    let path = s:find_file("build.flags") . "build.flags"
    if !filereadable(path)
        return
    endif

    let flags = readfile(path, '', 1)[0]
    let g:ale_cpp_clang_options = flags . " -Wno-pragma-once-outside-header"
endf

func! s:tabterm(cmd)
    tabe
    call termopen(a:cmd)
    startinsert
endf

func! s:find_file(name)
    let i = 0
    let prefix = ""
    while i < 10
        if filereadable(prefix . a:name)
            if prefix == ""
                return "./"
            else
                return prefix
            endif
        endif

        let prefix = "../" . prefix
        let i += 1
    endw
    return "./"
endf

