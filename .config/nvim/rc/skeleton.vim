" load skeleton for new file
if exists("g:loaded_rc_skeleton")
    finish
endif
let g:loaded_rc_skeleton = 1

"--------------------------------------------------------------------------
"-- interface -------------------------------------------------------------

let g:skeleton_dir = expand("<sfile>:p:h") . "/skeleton"

au BufRead skeleton call skeleton#filetype(expand("%:p:h:t"))
au BufNewFile * call skeleton#apply(expand("%:e"))

func! skeleton#filetype(source_base)
    call s:filetype(a:source_base)
endf

func! skeleton#apply(source_suffix)
    call s:apply(a:source_suffix)
endf

func! skeleton#cursor_line(marker, offset_lines)
    call s:goto_marker(a:marker)
    call s:move_cursor_line(a:offset_lines)
endf

"--------------------------------------------------------------------------
"-- implementation --------------------------------------------------------

func! s:filetype(source_base)
    let ft = s:ft(a:source_base)
    if ft != ''
        let &ft = ft
    endif
endf

func! s:apply(source_suffix)
    let alias = s:alias(a:source_suffix)
    if alias != ''
        return s:apply(alias)
    endif

    call s:load_skeleton(a:source_suffix)
    call s:run_script(a:source_suffix)
endf

func! s:goto_marker(marker)
    call search(a:marker)
    normal dd
endf

func! s:move_cursor_line(offset)
    if a:offset < 0
        exe "normal" repeat("k", -a:offset)
    elseif a:offset > 0
        exe "normal" repeat("j",  a:offset)
    endif
endf


func! s:ft(source_base)
    return s:first_line_if_exists(s:ft_path(a:source_base))
endf

func! s:alias(source_suffix)
    return s:first_line_if_exists(s:alias_path(a:source_suffix))
endf

func! s:load_skeleton(source_suffix)
    let path = s:skel_path(a:source_suffix)
    if filereadable(path)
        silent exe "1r" path
        silent exe "0d"
    endif
endf

func! s:run_script(source_suffix)
    let path = s:script_path(a:source_suffix)
    if filereadable(path)
        exe "source" path
    endif
endf


func! s:first_line_if_exists(path)
    if filereadable(a:path)
        return readfile(a:path, '', 1)[0]
    else
        return ''
    endif
endf


func! s:alias_path(source_suffix)
    return s:synthesis_path(a:source_suffix, "alias")
endf

func! s:script_path(source_suffix)
    return s:synthesis_path(a:source_suffix, "script.vim")
endf

func! s:skel_path(source_suffix)
    return s:synthesis_path(a:source_suffix, "skeleton")
endf

func! s:ft_path(source_base)
    return s:synthesis_path(a:source_base, "ft")
endf

func! s:synthesis_path(base, suffix)
    return g:skeleton_dir . "/" . a:base . "/" . a:suffix
endf

