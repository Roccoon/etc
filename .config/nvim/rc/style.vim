" style: look & feel configuration
if exists("g:loaded_rc_style")
    finish
endif
let g:loaded_rc_style = 1

set background=dark

" better vertical split style
set fillchars+=vert:│
hi clear VertSplit
hi VertSplit ctermfg=246 guifg=#a89984

" no number, but add a left margin
set nonumber
set norelativenumber
set ruler
set foldcolumn=1
hi clear FoldColumn
hi FoldColumn ctermfg=235 guifg=#282828
" should be hi link FoldColumn CursorLineNr

" some highlights modification
"hi Conceal ctermbg=232 ctermfg=69

