" xlua: extended lua
if exists("g:loaded_rc_xlua")
    finish
endif
let g:loaded_rc_xlua = 1

au ColorScheme *.lua call <SID>highlight()

func! s:highlight()
    " table access highlighting
    syn match xlua_table /\zs\I\i*\(\s*\.\s*\I\i*\)*\s*\ze[.:][^.]/ nextgroup=xlua_table_key,xlua_table_method skipwhite
    syn match xlua_table  /\zs\(\s*\.\s*\I\i*\)*\s*\ze[.:][^.]/ nextgroup=xlua_table_key,xlua_table_method skipwhite
    hi def link xlua_table luaTable

    syn match xlua_table_key /\.\s*\I\i*/ contained contains=xlua_table_key_dot
    hi def link xlua_table_key luaFunc

    syn match xlua_table_key_dot /\./ contained
    hi def link xlua_table_key_dot xlua_table

    syn match xlua_table_method /:\s*\I\i*/ contained
    hi def link xlua_table_method xlua_table_key

    " fix highlighting of ".."
    syn match luaBlock "\.\."
endf

