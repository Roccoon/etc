" Name: Header Text
" Description: Provide a command HeaderTXT to create a pure text file that
"              contains highlighting information
if exists("g:loaded_header_text")
    finish
endif
let g:loaded_header_text = 1

au BufRead *.txt exe matchstr(getline("1"), "\" Execute: \\zs.*")
au BufRead *.txt normal zM
au BufWritePost *.txt exe matchstr(getline("1"), "\" Execute: \\zs.*")
au BufNewFile *.txt HeaderTXT
au BufNewFile *.txt normal zM
exe "com! HeaderTXT 0r " . expand("<sfile>:p:h") . "/header.txt"

