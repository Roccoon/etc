" extended motion keys for bulk motion and quick motion in insert mode
if exists("g:loaded_rc_xmotion")
    finish
endif
let g:loaded_rc_xmotion = 1

cmap II <Home>
cmap AA <End>

" progressive indent
vnoremap > >gv
vnoremap < <gv

" scroll but keep cursor static (as to the eye)
nnoremap j gj<C-e>
nnoremap k gk<C-y>
inoremap JJ <C-o>gj<C-o><C-e>
inoremap KK <C-o>gk<C-o><C-y>

" jump list
nnoremap <C-b> <C-o>
nnoremap <C-d> <C-i>

imap <expr> <M-[> repeat("\<left>", &ts)
imap <expr> <M-]> repeat("\<right>", &ts)
nmap <expr> <M-[> repeat("\<left>", &ts)
nmap <expr> <M-]> repeat("\<right>", &ts)
xmap <expr> <M-[> repeat("\<left>", &ts)
xmap <expr> <M-]> repeat("\<right>", &ts)

imap <expr> <M-{> repeat("\<up>", &so)
imap <expr> <M-}> repeat("\<down>", &so)
nmap <expr> <M-{> repeat("\<up>", &so)
nmap <expr> <M-}> repeat("\<down>", &so)
xmap <expr> <M-{> repeat("\<up>", &so)
xmap <expr> <M-}> repeat("\<down>", &so)

