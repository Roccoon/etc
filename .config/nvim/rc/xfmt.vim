" xfmt: reformat code
if exists("g:loaded_rc_xfmt")
    finish
endif
let g:loaded_rc_xfmt = 1

nnoremap <F12> :call <SID>format()<CR>
imap <F12> <C-o><F12>

func! s:format()
    if &filetype == "rust"
        call s:wexec("rustfmt --write-mode overwrite --skip-children %")
    else
        echo "format by gg=G"
        silent normal mtgg=G`t
    endif
endf

func! s:wexec(cmd)
    w
    silent exe "!" . expand(a:cmd)
    e
    w
endf

