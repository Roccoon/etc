" cpp
if exists("b:current_syntax")
  finish
end

syn match extracpp_init_list /\v\w+(\s*\{)@=/

syn match extracpp_using_type /\v(<using>\s+\w+\s*[=]\s*)@<=\w+/
syn match extracpp_type_var /\v(<const>\s+)@<=\w+/
syn match extracpp_type_var /\v<\w+>(\s+const\s*[&*])@=/
syn match extracpp_type_var /\v(\.\s*)@<!<\w+>(\s*[&*])@=/
syn match extracpp_qualified_type /\v(::\s*)@<=\w+/
syn match extracpp_struct_type /\v(<(struct|class|enum)>\s+)@<=\w+/

syn match extracpp_type_suffix /\v<\w+_type>/
syn match extracpp_function /\v\w+\(@=/
syn match extracpp_template /\v\w+\<@=/
syn match extracpp_dot_chain_arrow /\v(\w+\s*)@<=-\>/
syn match extracpp_dot_chain_dot /\v(\w+\s*)@<=\./
syn match extracpp_dot_chain /\v\w+(\s*(\.|-\>))@=/
syn match extracpp_namespace_separator /\v::/
syn match extracpp_namespace /\v\w+(\s*::)@=/
syn match extracpp_namespace /\v(<namespace>\s+)@<=\w+(\s*::\s*\w+)*/

hi link extracpp_function giumo_function
hi link extracpp_template giumo_template
hi link extracpp_dot_chain giumo_weak_identifier
hi link extracpp_dot_chain_dot giumo_weak_identifier
hi link extracpp_dot_chain_arrow giumo_arrow
hi link extracpp_namespace giumo_namespace
hi link extracpp_namespace_separator giumo_namespace
hi link extracpp_type_suffix giumo_type

hi link extracpp_qualified_type giumo_type
hi link extracpp_struct_type giumo_type
hi link extracpp_type_var giumo_type
hi link extracpp_using_type giumo_type

hi link extracpp_init_list giumo_initialization

hi giumo_template guifg=#01efac
hi giumo_function guifg=#23ff4c
hi giumo_type guifg=#10c4ea
hi giumo_namespace guifg=#2b899d
hi giumo_initialization guifg=#8d8fff
hi giumo_weak_identifier guifg=#b39e69
hi giumo_arrow guifg=#92b97e

hi clear type
hi link type giumo_type

