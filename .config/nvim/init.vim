set shell=sh
set runtimepath+=/usr/share/vim/vimfiles
runtime rc/journal.vim
runtime! plugins.vim
runtime! rc/*.vim

filetype plugin indent on
set autoindent
set cinkeys-=0#             " never ever unindent preprocessor directives
set formatoptions-=tc       " never ever automatically break lines
set formatoptions+=B        " two CJK characters are joined without spaces in-between
set formatoptions+=m        " breakline support for CJK characters

set smartcase
set ignorecase

set hidden
set undofile

set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set shiftround

set wrap
set linebreak
set breakindent
set breakindentopt=shift:0
let &showbreak = "  ↪  "
set breakat-=*
set breakat-=/
set breakat-=+
set breakat-=-
set breakat-=.

set textwidth=0
set colorcolumn=76
set sidescrolloff=8
set sidescroll=1
set scrolloff=4

set conceallevel=2
set concealcursor=nv

set nu
set rnu
" write with sudo
cmap W!! w !sudo -u npsudo sudo tee "%"

nmap XXXX ZQ

nmap <C-f> <C-w>f
" FIXME: need the terminal to support "fixterms spec", otherwise C-i is the
" same as TAB key.
"nmap <C-i> <C-w>i
nmap gp `[v`]
map Y y$

