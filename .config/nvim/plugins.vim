let plugin_base = expand('<sfile>:p:h') . "/third-party"

call plug#begin(plugin_base)

"---- colorschemes
Plug 'morhetz/gruvbox'

"---- subsystems
Plug 'mhinz/vim-startify'               " Completely revamped startup screen
Plug 'blindFS/vim-taskwarrior'          " taskwarrior integration (task management)
Plug 'vimwiki/vimwiki'                  " personal wiki

"---- ide features
Plug 'tpope/vim-commentary'             " add/remove comments
Plug 'tpope/vim-fugitive'               " git support
Plug 'ctrlpvim/ctrlp.vim'               " fuzzy finder
Plug 'terryma/vim-expand-region'        " +/_ to select larger/smaller region
Plug 'w0rp/ale'                         " linter support

"---- enhancements
Plug 'tpope/vim-repeat'                 " allow other plugins to repeat by '.' atomically
Plug 'tpope/vim-surround'               " change/add surround pairs: `cs[{` `ysiW]`
Plug 'tpope/vim-unimpaired'             " paired operation: `]b` to next buffer, `]n` to next git conflict
Plug 'tommcdo/vim-exchange'             " exchange two text segments: `cxiw`
Plug 'simnalamburt/vim-mundo'           " visualize undo tree
Plug 'junegunn/vim-easy-align'          " align stuff

"---- enhancement: text objects
Plug 'kana/vim-textobj-user'            " support user defined text objects
Plug 'kana/vim-textobj-line'            " il/al
Plug 'kana/vim-textobj-entire'          " ie/ae
Plug 'kana/vim-textobj-indent'          " ii/ai
Plug 'Julian/vim-textobj-variable-segment' " iv/av    underscores, camel cases
Plug 'sgur/vim-textobj-parameter'       " i,/a,
Plug 'vimtaku/vim-textobj-sigil'        " ig/ag    perl sigil
Plug 'whatyouhide/vim-textobj-xmlattr'  " ix/ax    xml attributes
Plug 'kana/vim-textobj-datetime'        " idd/idt/idz/idf    ISO-8601 date/time/zone/full
Plug 'glts/vim-textobj-comment'         " ic/ac/aC -> rebind to im/am/aM
Plug 'Chun-Yang/vim-textobj-chunk'      " ic/ac    the block that starts from current line

"---- language support
Plug 'dag/vim-fish'                     " fish shell
Plug 'tpope/vim-afterimage'             " edit image file
Plug 'tikhomirov/vim-glsl'              " glsl
Plug 'vim-scripts/mojo.vim'             " perl Mojolicious template highlighting in __DATA__ part of perl file
Plug 'derekwyatt/vim-scala'             " Scala
"Plug 'cjxgm/nvim-nim'                   " nim
Plug 'othree/yajs.vim'                  " es6
Plug 'othree/es.next.syntax.vim'        " es7
Plug 'JulesWang/css.vim'                " css3
Plug 'rust-lang/rust.vim'               " rust syntax
Plug 'racer-rust/vim-racer'             " rustdoc syntax and rust completion
Plug 'cespare/vim-toml'                 " toml
Plug 'rhysd/vim-rustpeg'                " rustpeg

call plug#end()

" setup startify
let g:startify_custom_indices = ['a', 'd', 'w', 'f', 'z', 'x', 'c', 'r', '`']
let g:startify_custom_header = ['                neovim']

" setup easy align
xmap <CR> <Plug>(EasyAlign)
nmap <CR> <Plug>(EasyAlign)

" setup ale
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_enter = 0
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 1
let g:ale_sign_error = '×'
let g:ale_sign_warning = '▵'
" C++ options will be overriden in rc/xrun
let g:ale_cpp_clang_options = '-Wall -Wextra -std=c++1z -Wno-pragma-once-outside-header'
let g:ale_linters = {
            \ 'cpp': ['clang'],
            \ }

" setup commentary (only for debugging-purposed comments)
vmap / gcgv
au FileType nim let b:commentary_format="#%s"
au FileType cpp let b:commentary_format="//%s"
au FileType c let b:commentary_format="//%s"

" rebind custom text objects
let g:textobj_comment_no_default_key_mappings = 1
omap im <Plug>(textobj-comment-i)
xmap am <Plug>(textobj-comment-a)
xmap aM <Plug>(textobj-comment-big-a)

" setup task warrior
let g:task_default_prompt = ['description', 'project', 'due', 'depends', 'tag', 'priority']
let g:task_rc_override = 'rc.defaultwidth=0 rc.defaultheight=0 rc.detection=no'
nmap <Leader>w :TW<CR>

" setup ctrlp
let g:ctrlp_regexp = 1
let g:ctrlp_cmd = 'CtrlPMixed'

