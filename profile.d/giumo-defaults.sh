export EDITOR="nvim"
export TERMINAL="gnome-terminal"
export DIFFPROG="$EDITOR"
export GIT_EDITOR="$EDITOR"
export PAGER="less -x4 -RinS"
#export MANPAGER="nvim -c 'set ft=man' -"

